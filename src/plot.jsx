import React from "react";
import Plot from "react-plotly.js";

export default function Figure(props) {
  return (
    <Plot
      data={[
        {
          x: props.data.freq,
          y: props.data.S_v,
          name: "PSD",
          marker: { color: "black" }
        }
      ]}
      layout={{
        xaxis: {
          title: "f (Hz)",
          type: "log"
        },
        yaxis: {
          title: "V<sub>PSD</sub> (V<sup>2</sup>/Hz)",
          type: "log"
        },
        autosize: true,
        margin: {
          l: 75,
          r: 25,
          b: 50,
          t: 25,
          pad: 0
        }
      }}
      useResizeHandler={true}
      className="figure"
    />
  );
}
