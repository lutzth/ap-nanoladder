import React from "react";
import ReactDOM from "react-dom";

import Experiment from "./experiment";

ReactDOM.render(<Experiment />, document.getElementById("root"));
